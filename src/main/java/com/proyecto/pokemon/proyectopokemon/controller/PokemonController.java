package com.proyecto.pokemon.proyectopokemon.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.ls.LSResourceResolver;

import com.proyecto.pokemon.proyectopokemon.entidad.Principal;
import com.proyecto.pokemon.proyectopokemon.entidad.Detalle;
import com.proyecto.pokemon.proyectopokemon.entidad.Detalle2;
import com.proyecto.pokemon.proyectopokemon.entidad.Species;
import com.proyecto.pokemon.proyectopokemon.entidad.Results;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/prueba")
@CrossOrigin("*")

public class PokemonController {

	@GetMapping("/obtenerPokemon")
	public Object getAllPokemon() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("user-agent", null);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		String url = "https://pokeapi.co/api/v2/pokemon?limit=893&offset=0";
		ResponseEntity<Principal> principalResponse = restTemplate.exchange(url, HttpMethod.GET, entity,
				Principal.class);
		Principal principal = principalResponse.getBody();
		return principal;
	}

	@GetMapping("/obtenerDetallexPokemon")
	public Object getUrl(@RequestHeader String requestUrl) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("user-agent", null);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<Detalle> detalleResponse = restTemplate.exchange(requestUrl, HttpMethod.GET, entity,
				Detalle.class);
		Detalle detalle = detalleResponse.getBody();
		return detalle;
	}

	@GetMapping("/obtenerDetallexPokemon2")
	public Object getUrl2(@RequestHeader String requestUrl) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("user-agent", null);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<Detalle2> detalle2Response = restTemplate.exchange(requestUrl, HttpMethod.GET, entity,
				Detalle2.class);
		Detalle2 detalle2 = detalle2Response.getBody();
		return detalle2;
	}

}
