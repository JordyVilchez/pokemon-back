package com.proyecto.pokemon.proyectopokemon.entidad;

import java.util.List;

public class Principal {
	private Integer count;
	private String next;
	private List<Results> results;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public List<Results> getResults() {
		return results;
	}

	public void setResults(List<Results> results) {
		this.results = results;
	}
}
