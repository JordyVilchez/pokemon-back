package com.proyecto.pokemon.proyectopokemon.entidad;

public class Detalle2 {
	private Integer base_happiness;
	private Integer capture_rate;
	private EvolvesFromSpecies evolves_from_species;

	public Integer getBase_happiness() {
		return base_happiness;
	}

	public void setBase_happiness(Integer base_happiness) {
		this.base_happiness = base_happiness;
	}

	public Integer getCapture_rate() {
		return capture_rate;
	}

	public void setCapture_rate(Integer capture_rate) {
		this.capture_rate = capture_rate;
	}

	public EvolvesFromSpecies getEvolves_from_species() {
		return evolves_from_species;
	}

	public void setEvolves_from_species(EvolvesFromSpecies evolves_from_species) {
		this.evolves_from_species = evolves_from_species;
	}
}
