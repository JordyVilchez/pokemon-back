package com.proyecto.pokemon.proyectopokemon.entidad;

public class Ability {
	private DetailAbility ability;

	public DetailAbility getAbility() {
		return ability;
	}

	public void setAbility(DetailAbility ability) {
		this.ability = ability;
	}
}
