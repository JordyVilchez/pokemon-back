package com.proyecto.pokemon.proyectopokemon.entidad;

import java.util.List;

public class Detalle {
	private Integer id;
	private List<Ability> abilities;
	private Species species;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Ability> getAbilities() {
		return abilities;
	}

	public void setAbilities(List<Ability> abilities) {
		this.abilities = abilities;
	}

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}
}
